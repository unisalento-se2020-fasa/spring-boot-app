package it.unisalento.springbootapp.singerimpl;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

import it.unisalento.springbootapp.isinger.ISinger;

@Component
@Primary
public class Lennon implements ISinger {
	
	public void sing() {
		System.out.println("Image all the people sharing all the world!!");
	}
	
}


